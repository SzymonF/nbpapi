<?php

namespace App\Controller;

use App\Service\NBPService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NBPController extends AbstractController
{
    /**
     * @Route("/nbp", name="nbp")
     */
    public function index(NBPService $NBPService): Response
    {
        $dataSynchronize = $NBPService->synchronizeCurrency();
        return $this->json([
            'message' => 'Wykonano synchronizacje',
            'details'=>$dataSynchronize
        ]);
    }
}
