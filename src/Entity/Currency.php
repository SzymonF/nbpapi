<?php

namespace App\Entity;

use App\Repository\CurrencyRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 */
class Currency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *     min = 1,
     *     max= 255
     * )
     * @Assert\NotBlank()
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=3)
     * @Assert\Length(3)
     * @Assert\Type(type="alpha")
     * @Assert\NotBlank()
     */
    private ?string $currency_code;

    /**
     *
     * @ORM\Column(type="decimal", precision=11, scale=8)
     * @Assert\Regex("/^\d{1,3}\.\d{1,8}?$/")
     * @Assert\NotBlank()
     */
    private ?string $exchange_rate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currency_code;
    }

    public function setCurrencyCode(string $currency_code): self
    {
        $this->currency_code = $currency_code;

        return $this;
    }

    public function getExchangeRate(): ?string
    {
        return $this->exchange_rate;
    }

    public function setExchangeRate(string $exchange_rate): self
    {
        $this->exchange_rate = $exchange_rate;

        return $this;
    }

    public function __toString()
    {
        return 'KOD WALUTY:' . $this->getCurrencyCode();
    }
}
