<?php

namespace App\Service;

use Exception;

class NBPService
{

    private NBPDataService $NBPDataService;
    private CurrencyService $currencyService;

    public function __construct(NBPDataService $NBPDataService, CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
        $this->NBPDataService = $NBPDataService;
    }

    public function synchronizeCurrency(): array
    {
        return $this->currencyService->operateCurrencies(
            $this->prepareRates(
                $this->NBPDataService->getData()
            )
        );
    }

    private function prepareRates(array $dataAPINBP): array
    {
        if (isset($dataAPINBP[0]['rates'])) {
            return $dataAPINBP[0]['rates'];
        }
        throw new Exception('Zły format danych');

    }


}