<?php

namespace App\Service;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidationService
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($object): ConstraintViolationListInterface
    {
        return $this->validator->validate($object);
    }

    /**
     * Example SingleRate
     * Array
        (
        [currency] => bat (Tajlandia)
        [code] => THB
        [mid] => 0.1209
        )

     */
    public function validateCurrency(array $singleRate): bool
    {

        return isset($singleRate['currency']) && isset($singleRate['code']) && isset($singleRate['mid']);
    }
}