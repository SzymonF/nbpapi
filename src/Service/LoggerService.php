<?php

namespace App\Service;

class LoggerService
{


    private array $logs = [];


    public function attachLog(array $log)
    {
        $this->logs[] = $log;
    }

    public function outputLogs(): array
    {
        return $this->logs;
    }
}