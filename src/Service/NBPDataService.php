<?php

namespace App\Service;

use Exception;

class NBPDataService
{
    private string $url = 'http://api.nbp.pl/api/exchangerates/tables/A/';
    private APIService $APIService;

    public function __construct(APIService $APIService)
    {
        $this->APIService = $APIService;
    }

    /**
     * @throws Exception
     */
    public function getData(): array
    {
        return $this->APIService->fetchDataGET($this->url);
    }
}