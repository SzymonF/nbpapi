<?php

namespace App\Service;

use App\Entity\Currency;
use App\Repository\CurrencyRepository;
use Doctrine\ORM\EntityManagerInterface;

class CurrencyService
{
    private array $existCurrencies;
    private array $newCurrency;
    private CurrencyRepository $currencyRepository;
    private ValidationService $validationService;
    private EntityManagerInterface $entityManager;
    private LoggerService $loggerService;

    public function __construct(CurrencyRepository     $currencyRepository,
                                EntityManagerInterface $entityManager,
                                ValidationService      $validationService,
                                LoggerService          $loggerService
    )
    {
        $this->loggerService = $loggerService;
        $this->validationService = $validationService;
        $this->entityManager = $entityManager;
        $this->currencyRepository = $currencyRepository;
    }

    public function operateCurrencies(array $currencies): array
    {

        $this->setExistCurrencies();
        foreach ($currencies as $singleCurrency) {
            if ($this->validationService->validateCurrency($singleCurrency)) {
                $this->setNewCurrency($singleCurrency);
                $this->analyzeCurrency();
            }
        }
        $this->entityManager->flush();
        return $this->loggerService->outputLogs();
    }


    private function setExistCurrencies(): void
    {
        $this->existCurrencies = $this->currencyRepository->findAll();
    }

    private function setNewCurrency(array $newCurrency): void
    {
        $this->newCurrency = $newCurrency;
    }

    private function analyzeCurrency(): void
    {
        $currency = $this->existCurrency();
        if (is_null($currency)) {
            $currency = $this->newCurrency();
        }
        $this->addCurrency($currency);
    }

    private function existCurrency(): ?Currency
    {
        foreach ($this->existCurrencies as $singleExistCurrency) {
            if ($this->equalCurrency($singleExistCurrency->getName(), $singleExistCurrency->getCurrencyCode())) {
                $singleExistCurrency->setExchangeRate($this->newCurrency["mid"]);
                return $singleExistCurrency;
            }
        }
        return null;
    }

    private function equalCurrency(string $existCurrencyName, string $existCurrencyCode): bool
    {
        return $this->newCurrency["currency"] === $existCurrencyName && $this->newCurrency['code'] === $existCurrencyCode;
    }


    private function newCurrency(): Currency
    {
        $currency = new Currency();
        $currency->setName($this->newCurrency["currency"]);
        $currency->setCurrencyCode($this->newCurrency['code']);
        $currency->setExchangeRate($this->newCurrency["mid"]);
        return $currency;
    }


    private function addCurrency(Currency $currency): void
    {
        $status = false;
        $error = $this->validationService->validate($currency);
        if (count($error) === 0) {
            $status = true;
            $this->entityManager->persist($currency);
        }
        $this->loggerService->attachLog(['status' => $status, 'obj' => (string)$currency, 'errors' => (string)$error]);

    }


}