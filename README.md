# Zadanie - opis

Założenia:

* aplikacja na symfony 5 lub najnowszy Laravel
* php 7.4
* mariadb/mysql jedna z nowszych


1. Wygenerowanie encji Currency z polami:

* id - uuid
* name - string - nazwa waluty
* currency_code - string - 
* exchange_rate - proszę dobrać typ wg. własnych doświadczeń - wartość kursu waluty
  względem złotówki (czyli np. dla EURO będzie to np. 1 Euro = 4,49 PLN, czyli chcemy w
  bazie mieć wartość 4,49 lub np. 449 (w zależności od podejścia))


2. Przygotowanie integracji z API NBP - http://api.nbp.pl/

   * integracja powinna połączyć się z API NBP i pobrać dane o aktualnych kursach walut (z
   tabeli A)

   * jeżeli dana waluta już istnieje, to powinna zostać zaktualizowana wartość exchange_rate

   * jeżeli dana waluta nie istnieje, powinna zostać dodana




# Uruchomienie

Należy uruchomić instancje symfony i wywołać adres /nbp. 

# Baza danych
* Name => varchar(255)
* currency_code => varchar(3) 

Według ISO 4217 currency_code zawsze ma 3 znaki, zastosowałem walidacje [alpha](https://www.php.net/manual/en/function.ctype-alpha.php).

* exchange_rate => DECIMAL(11,8) 
        
Uznałem, że po przecinku maksymalnie możemy dać 8 znaków, a przed 3. Uważam, że jest to wystarczająca liczba. Zastosowałem regex w celu walidacji.

Gdy będzie potrzeba wyświetlenia wartości na froncie, wtedy można uciąć nadmiarowe zera, lecz w tym przypadku mamy pewność, że przelicznik będzie dokładny.


